package ru.tsc.chertkova.tm;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.component.Bootstrap;

public class TaskManagerLogger {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
