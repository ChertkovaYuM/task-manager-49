package ru.tsc.chertkova.tm.exception.entity;

import ru.tsc.chertkova.tm.exception.AbstractException;

public final class StatusNotFoundException extends AbstractException {

    public StatusNotFoundException() {
        super("Error! Status not found...");
    }

}
