package ru.tsc.chertkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.tsc.chertkova.tm.util.TerminalUtil;

public final class ProjectCascadeRemoveCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-cascade-remove";

    @NotNull
    public static final String DESCRIPTION = "Project cascade remove.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CASCADE REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        getServiceLocator().getProjectEndpoint()
                .removeProjectById(new ProjectRemoveByIdRequest(getToken(), projectId));
    }

}
