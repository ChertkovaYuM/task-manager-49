package ru.tsc.chertkova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.dto.IAbstractRepositoryDTO;
import ru.tsc.chertkova.tm.model.dto.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractRepositoryDTO<M extends AbstractModelDTO>
        implements IAbstractRepositoryDTO<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepositoryDTO(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull M model) {
        entityManager.merge(model);
    }

    @Override
    public abstract void clear();

    @Override
    @NotNull
    public abstract List<M> findAll();

    @Override
    @Nullable
    public abstract M findById(@NotNull String id);

    @Override
    public abstract int getSize();

    @Override
    public abstract void removeById(@NotNull String id);

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

}
