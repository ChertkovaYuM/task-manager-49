package ru.tsc.chertkova.tm.exception.entity;

import ru.tsc.chertkova.tm.exception.AbstractException;

public final class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
