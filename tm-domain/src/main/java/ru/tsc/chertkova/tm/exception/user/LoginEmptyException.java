package ru.tsc.chertkova.tm.exception.user;

import ru.tsc.chertkova.tm.exception.field.AbstractFieldException;

public final class LoginEmptyException extends AbstractFieldException {

    public LoginEmptyException() {
        super("Error! Login is empty...");
    }

}
