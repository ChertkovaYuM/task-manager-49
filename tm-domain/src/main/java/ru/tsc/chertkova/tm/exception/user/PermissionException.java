package ru.tsc.chertkova.tm.exception.user;

import ru.tsc.chertkova.tm.exception.AbstractException;

public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}
