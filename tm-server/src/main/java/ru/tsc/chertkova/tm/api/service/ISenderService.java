package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.model.EntityLog;

public interface ISenderService {

    void send(@NotNull EntityLog entity);

    @NotNull
    EntityLog createMessage(@NotNull Object object, @NotNull String type);

}
